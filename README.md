# Proj2_Algorithms

Image upscaling project for COP4531 using two different upscaling implementations.
Resulting output will be in file 'output.txt' in your current working directory.  
  
**Running ./a.out after 'make test' throws this error when run on my server:**  
> ./a.out: error while loading shared libraries: libgtest.so.1.10.0: cannot open shared object file: No such file or directory  

_As a result, upsample_test.cpp may not work due to the inability to actually perform the gtest._
  
**upsample.cpp requires arguments to be passed in command line.  
Example:**  

> ./main sample1_input_image.txt  
  
Function durations are determined using the chrono library.  
  
**L1 metric evaluation is implemented in each function.  
To evaluate with L1 implementation, pass original image text file in command line as 2nd value in main arguments.  
Example:**  
>./main sample1_input_image.txt sample1_original_image.txt_

https://gitlab.com/KyleW20/proj2_algorithms/-/tree/master
