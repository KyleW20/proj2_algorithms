#include <iostream>
#include <fstream>
#include <cmath>
#include <chrono>
using namespace std;

int nearestNeighbor(const char * in, const char * out, const char * orig);
int bilinear(const char * in, const char * out, const char * orig);

int main(int argc, char *argv[])
{
	if(argc > 1){
		const char * in = argv[1];
		const char * out = "output.txt";
		const char * orig = argv[2];
		int nnTest = 0, bilinearTest = 0;
		
		auto start = chrono::high_resolution_clock::now();
		nnTest = nearestNeighbor(in, out, orig);
		auto end = chrono::high_resolution_clock::now() - start;
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end).count();
		cout << "Nearest Neighbor duration: " << duration << " micro-seconds" << endl;
	/*
		auto start = chrono::high_resolution_clock::now();
		bilinearTest = bilinear(in, out, orig);
		auto end = chrono::high_resolution_clock::now() - start;
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end).count();
		cout << "Bilinear duration: " << duration << " micro-seconds" << endl;
		*/
	}
	
	return 0;
}

int nearestNeighbor(const char * in, const char * out, const char * orig){
	ifstream infile;
	ofstream outfile;
	infile.open(in);
	int img[256][256];
	int imgOut[512][512];
	int original[512][512];
	int input = 0;
	char fill;
	int count = 0;
	int test = 0;
	
		
	//error checking for file before parsing
	if(infile.is_open()){
		for(int i = 0; i < 256; i++){
			for(int j = 0; j < 256; j++){
				infile >> input >> fill;
				img[i][j] = input;
			}
		}
		infile.close();
	}
	
	//Nearest-neighbor implementation

	for(int i = 0; i < 512; i++){
		for(int j = 0; j < 512; j++){
			//if the row is oddly numbered, fills in empty spaces
			if((i % 2) == 1){
				//if column is evenly numbered, fills in nearest neighbor
				if((count % 2) == 0){
					imgOut[i][j] = imgOut[i-1][j];
				}
				else{
					imgOut[i][j] = imgOut[i-1][j-1];
				}
			}
			else{
				//if column is oddly numbered, fills in nearest neighbor
				if((j % 2) == 1){
					imgOut[i][j] = imgOut[i][j-1];
				}
				else{
					//if column is even and row is even, then it retrieves original value from input image
					imgOut[i][j] = img[i/2][j/2];
				}
			}
		}

		count = 0;
	}
	
	infile.open(orig);
	if(infile.is_open()){
		for(int i = 0; i < 512; i++){
			for(int j = 0; j < 512; j++){
				infile >> input >> fill;
				original[i][j] = input;
			}
		}
		infile.close();
	}
	int sum = 0;
	
	for(int i = 0; i < 512; i++){
		for(int j = 0; j < 512; j++){
			sum = sum + (original[i][j] - imgOut[i][j]);
		}
	}
	cout << "L1 metric of current implementation: " << abs(sum) << endl;
	
	outfile.open(out);
	if(outfile.is_open()){
		for(int i = 0; i < 512; i++){
			for(int j = 0; j < 512; j++){
				outfile << imgOut[i][j];
				if(j != 511)
					outfile << ",";
			}
			outfile << endl;
		}
		test = 1;
		outfile.close();
	}
	return test;
}

int bilinear(const char * in, const char * out, const char * orig){
	ifstream infile;
	ofstream outfile;
	infile.open(in);
	int img[256][256];
	int imgOut[512][512];
	int original[512][512];
	int input = 0;
	char fill;
	int count = 0;
	int test = 0;
		
	//Bilinear implementation

	
	int xLinInt = 0, yLinInt = 0, edge = 512;
	
	if(infile.is_open()){
		for(int i = 0; i < 256; i++){
			for(int j = 0; j < 256; j++){
				infile >> input >> fill;
				img[i][j] = input;
			}
		}
		infile.close();
	}
	
	
	for(int i = 0; i < 512; i++){
		for(int j = 0; j < 512; j++){
			if(((i % 2) == 0) && ((j % 2) == 0)){
				//if column is even and row is even, then it retrieves original value from input image
				imgOut[i][j] = img[i/2][j/2];
			}
		}
	}
	
	for(int i = 0; i < 512; i++){
		for(int j = 0; j < 512; j++){
			//if colmun is oddly numbered, fills in value with linear interpolation between imgOut[j-1] and imgOut[j+1]
			if(((j % 2) == 1) && ((i % 2) == 0) && (j != (edge - 1))){
				xLinInt = floor(imgOut[i][j-1] + .5*(imgOut[i][j+1] - imgOut[i][j-1]));
				
				imgOut[i][j] = xLinInt;
			}
			else if(j == (edge - 1)){
				imgOut[i][j] = imgOut[i][j - 1];
			}
		}
	}

	for(int i = 0; i < 512; i++){
		for(int j = 0; j < 512; j++){
			//if colmun is oddly numbered, fills in value with linear interpolation between imgOut[j-1] and imgOut[j+1]
			if(((i % 2) == 1) && (i != (edge - 1))){
				yLinInt = floor(imgOut[i-1][j] + .5*(imgOut[i+1][j] - imgOut[i-1][j]));
				
				imgOut[i][j] = yLinInt;
			}
			else if(i == (edge - 1)){
				imgOut[i][j] = imgOut[i - 1][j];
			}
		}
	}
	
	
	infile.open(orig);
	if(infile.is_open()){
		for(int i = 0; i < 512; i++){
			for(int j = 0; j < 512; j++){
				infile >> input >> fill;
				original[i][j] = input;
			}
		}
		infile.close();
	}
	int sum = 0;
	
	for(int i = 0; i < 512; i++){
		for(int j = 0; j < 512; j++){
			sum = sum + (original[i][j] - imgOut[i][j]);
		}
	}
	cout << "L1 metric of current implementation: " << abs(sum) << endl;
	
	
	outfile.open(out);
	if(outfile.is_open()){
		for(int i = 0; i < 512; i++){
			for(int j = 0; j < 512; j++){
				outfile << imgOut[i][j];
				if(j != 511)
					outfile << ",";
			}
			outfile << endl;
		}
		test = 1;
		outfile.close();
	}
	return test;

}