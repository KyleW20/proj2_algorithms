#include <gtest/gtest.h>
#include "upsample.cpp"

TEST(NeighborTest, nnRuns)
{
	const char * in = "sample1_input_image.txt";
	const char * out = "output.txt";
	const char * orig = "sample1_original_image.txt";
	EXPECT_EQ(1, nearestNeighbor(in, out, orig));
}

TEST(NeighborTest2, nnRuns)
{
	const char * in = "sample1_input_image.txt";
	const char * out = "";
	const char * orig = "sample1_original_image.txt";
	EXPECT_EQ(1, nearestNeighbor(in, out, orig));
}

TEST(BilinearTest, biRuns)
{
	const char * in = "sample1_input_image.txt";
	const char * out = "output.txt";
	const char * orig = "sample1_original_image.txt";
	EXPECT_EQ(1, bilinear(in, out, orig));
}

TEST(BilinearTest2, biRuns)
{
	const char * in = "sample1_input_image.txt";
	const char * out = "";
	const char * orig = "sample1_original_image.txt";
	EXPECT_EQ(1, bilinear(in, out, orig));
}

int main(int argc, char* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}