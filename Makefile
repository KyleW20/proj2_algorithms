#	mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
#	current_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))
all: upsample.o
	g++6 -lm upsample.o -o main

upsample.o: upsample.cpp
	g++6 -c upsample.cpp
	
test:
#	git clone https://github.com/google/googletest.git
#	cp $(PWD)/CMakeUpdate.txt $(PWD)/googletest/CMakeUpdate.txt
#	cat $(PWD)/googletest/CMakeUpdate.txt $(PWD)/googletest/CMakeLists.txt > $(PWD)/googletest/output.txt
#	mv $(PWD)/googletest/CMakeLists.txt $(PWD)/googletest/outdated.txt
#	mv $(PWD)/googletest/output.txt $(PWD)/googletest/CMakeLists.txt
#	mkdir $(PWD)/googletest/build
#	cd build; cmake ..; make
	g++ -std=c++11 -I $(PWD)/googletest/googletest/include/ -L $(PWD)/googletest/build/lib upsample_test.cpp -lgtest -lpthread -o test
	./test
	
coverage:
	g++ -std=c++11 -fprofile-arcs -ftest-coverage -fPIC upsample.cpp -o coverage
	./coverage sample1_input_image.txt sample1_original_image.txt
	~/.local/bin/gcovr -r . --html > coverage.html
	~/.local/bin/gcovr -r . --html --html-details -o coverage-complete.html
	
clean:
	rm *.o *.html *.gcno *.gcda main test a.out